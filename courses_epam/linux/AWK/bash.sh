#!/bin/env bash
RAND_ARRAY=($(awk 'BEGIN { for (i=1;i<200;i++)print int (101*rand())}'))
COUNT=${#RAND_ARRAY[@]}
echo limits
for i in $(seq 0 10 100); do
 let i_MAX=i+9
 SUM=0
 j=0
 while [ $j -lt $COUNT ]; do
  if [[ ${RAND_ARRAY[j]} -ge $i && ${RAND_ARRAY[j]} -le $i_MAX ]];then
   let SUM=SUM+1
  fi
  let j=j+1
 done
 echo $i - $i_MAX : $SUM
done
