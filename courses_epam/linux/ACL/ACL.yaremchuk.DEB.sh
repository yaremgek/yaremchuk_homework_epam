#!/bin/bash

if ! [ -d /opt/projects/ ]; then mkdir /opt/projects; fi;
if ! [ -d /opt/projects/proj1 ]; then mkdir /opt/projects/proj1; fi;
if ! [ -d /opt/projects/proj2 ]; then mkdir /opt/projects/proj2; fi;
if ! [ -d /opt/projects/proj3 ]; then mkdir /opt/projects/proj3; fi;

for i in Proj1RW Proj2RW Proj3RW Proj3R Proj2R Proj1R;
do
        groupadd $i;
done

for i in R1 R2 R3 R4 R5 A1 A2 A3 A4;
do
        adduser --force-badname --system --shell $BASH $i;
done

PROJ1RW=(R2 R3 R5 A1);
PROJ2RW=(R1 R5 A1);
PROJ3RW=(R1 R2 R4 A2);
PROJ1R=(A4);
PROJ2R=(A2 A3);
PROJ3R=(A1 A4);
for i in ${PROJ1RW[@]};
do
        usermod -a -G Proj1RW $i
done

for i in ${PROJ2RW[@]};
do
        usermod -a -G Proj2RW $i
done

for i in ${PROJ3RW[@]};
do
        usermod -a -G Proj3RW $i
done

for i in ${PROJ1R[@]};
do
        usermod -a -G Proj1R $i
done

for i in ${PROJ2R[@]};
do
        usermod -a -G Proj2R $i
done

for i in ${PROJ3R[@]};
do
        usermod -a -G Proj1R $i
done


setfacl -m g:Proj1RW:x /opt/
setfacl -m g:Proj1RW:x /opt/projects/
setfacl -m g:Proj1RW:rwx /opt/projects/proj1/
setfacl -m g:Proj1R:x /opt/
setfacl -m g:Proj1R:x /opt/projects/
setfacl -m g:Proj1R:rx /opt/projects/proj1

setfacl -m g:Proj2RW:x /opt/
setfacl -m g:Proj2RW:x /opt/projects/
setfacl -m g:Proj2RW:rwx /opt/projects/proj2/
setfacl -m g:Proj2R:x /opt/
setfacl -m g:Proj2R:x /opt/projects/
setfacl -m g:Proj2R:rx /opt/projects/proj2

setfacl -m g:Proj3RW:x /opt/
setfacl -m g:Proj3RW:x /opt/projects/
setfacl -m g:Proj3RW:rwx /opt/projects/proj3/
setfacl -m g:Proj3R:x /opt/
setfacl -m g:Proj3R:x /opt/projects/
setfacl -m g:Proj3R:rx /opt/projects/proj3

