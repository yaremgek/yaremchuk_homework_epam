import datetime
import time
from datetime import datetime
from functools import wraps


def delay(func):
    @wraps(func)
    def wrapper(*args,**kwargs):
        time.sleep()
        res = func(*args,**kwargs)
        return res
    return wrapper
