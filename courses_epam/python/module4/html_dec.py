def bold(func):
    def wrapper(*args,**kwargs):
        res = func(*args, **kwargs)
        return '<b>{}</b>'.format(res)
    return wrapper

def underline(func):
    def wrapper(*args,**kwargs):
        res = func(*args, **kwargs)
        return '<u>{}</u>'.format(res)
    return wrapper

def italic(func):
    def wrapper(*args,**kwargs):
        res = func(*args, **kwargs)
        return '<i>{}</i>'.format(res)
    return wrapper
