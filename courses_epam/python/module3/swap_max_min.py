def swap_max_and_min(list_u):
    if len(list_u) > 0 and len(list_u) == len(set(list_u)):
        if list_u.index(max(list_u)) > list_u.index(min(list_u)):
            list_u[list_u.index(max(list_u))], list_u[list_u.index(min(list_u))] = list_u[list_u.index(min(list_u))],\
                                                                                   list_u[list_u.index(max(list_u))]
        else:
            list_u[list_u.index(min(list_u))], list_u[list_u.index(max(list_u))] = list_u[list_u.index(max(list_u))], \
                                                                                   list_u[list_u.index(min(list_u))]
    else:
        raise ValueError
    return list_u
