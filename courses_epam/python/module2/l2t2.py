#!/bin/env python36
import sys
from sys import argv
from math import sqrt
args_count = len(sys.argv)
if args_count == 4:
    side_a = int(argv[1])
    side_b = int(argv[2])
    side_c = int(argv[3])
    p_var = (side_a+side_b+side_c) / 2
    s_var = sqrt(abs(p_var*(p_var-side_a)*(p_var-side_b)*(p_var-side_c)))
    print('Area of a triangle = ', s_var)
else:
    print('Pls enter three args for this script and may be you are be happy')

