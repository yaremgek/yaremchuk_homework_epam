#!/bin/env python36

import math
x = int(input("Enter a number: "))
#(−15,12]∪(14,17)∪[19,+∞)
first_range = range(-15,12)
second_range = range(15,17)

if x in first_range or x in second_range or x > 19:
	print('True')
else:
	print('False')
